package Patefon;

use warnings;
use strict;
use CGI qw/param/;
use Fcntl qw(:DEFAULT :flock);
use File::Spec;

BEGIN {
    use Exporter;
    our (@ISA, @EXPORT);
    @ISA = qw(Exporter);
    @EXPORT = qw(
	&lock_the_room
	&change_the_plate
	&listen_the_plate

	%patefons_knobs
	%nowPlaying

	$room_toChange
    );
}
param('nowPlaying')	and
our %nowPlaying				= param('nowPlaying') =~ /"(\d+)":"(\d+)"/g;

param('changeThePlate')	and
our ( $room_toChange, $plate_toChange )	= param('changeThePlate') =~ /"(\d+)":"(\d+)"/;

our %patefons_knobs = (
	'sample_rate'	=> 1,				# (in seconds) time for waiting after each 'listen' iterate
	'maxSleeping'	=> 20,				# (in seconds) maximum time for listening
	'path_to_rooms'	=> '.patefon_rooms',		# relatively your script or absolute
	'errors'	=> [],
	'handlers'	=> {},				# subs to start when 'have heard new plate' — 'port' => '\sub'
	'ChngHandlers'	=> {},				# subs to start when 'change the plate' — 'port' => '\sub'
);

{
	my ( $locked, $door );

	sub lock_the_room {
		my $room = shift or do { push @{$patefons_knobs{errors}}, qq{[lock_the_room]: I need number of a room to lock}; return 0 };
		$patefons_knobs{path_to_rooms} or do { push @{$patefons_knobs{errors}}, qq{[lock_the_room]: There's no path to rooms}; return 0 };
		$locked = 0;

		if ( open $door, "<", File::Spec->catfile($patefons_knobs{path_to_rooms}, $room, 'door' ) ) {
			flock ( $door, LOCK_EX );
			return $locked = 1;
		}
		push @{$patefons_knobs{errors}}, 
			-d File::Spec->catfile($patefons_knobs{path_to_rooms}, $room )		? 
				qq{[lock_the_room]: There is no door in the patefon's room} 	:
				qq{[lock_the_room]: There is no patefon's room at all};
		return 0;
	}

	sub change_the_plate {

		$room_toChange or do{ push @{$patefons_knobs{errors}}, qq{[change_the_plate]: I need room number}; return 0 };

# 		parse banderol and unshield all & and :
		my %banderol;
# push @{$patefons_knobs{errors}}, 'BANDEROL: ' . param('banderol');
		my $b;
		if ( $b = param('banderol') ) {
			$b =~ s/^{|}$//g;
			%banderol = map {
				my ( $s1, $s2 ) = /"(.+)":"(.+)"/;
				$s1 =~ s/\\,/,/g;
				$s2 =~ s/\\,/,/g;
				($s1, $s2);
			} $b =~ m/.+?[^\\](?:,|$)/g;
		}
# printf "%-15s%-15s\n",$_,$banderol{$_} foreach ( keys %banderol ); return 1;

		unless ( $locked ) {
			lock_the_room( $room_toChange ) or
				do { push @{$patefons_knobs{errors}}, qq{[change_the_plate]: I can't lock the room}; return 0 };
		}

		if ( opendir my $room, File::Spec->catfile($patefons_knobs{path_to_rooms}, $room_toChange ) ) {

			my @plates = sort ( 0, grep { /^\d+$/ } readdir $room);		# maybe someone creates a new and hasn't deleted old one
			my $new_plate= $plates[-1] + 1;

# 			start outer handler
			my $handlerResult = 0;
			if ( ref ${$patefons_knobs{ChngHandlers}}{$room_toChange} eq 'CODE' ) {
				$handlerResult = &{$patefons_knobs{ChngHandlers}{$room_toChange}}( $room_toChange, $new_plate, \%banderol )
			}

			unless ( $handlerResult ) { push @{$patefons_knobs{errors}}, qq{[change_the_plate]: handler has returned false}; return 0 }

			open ( my $file, ">", File::Spec->catfile($patefons_knobs{path_to_rooms}, $room_toChange, $new_plate ) ) or
				do { push @{$patefons_knobs{errors}}, qq{[change_the_plate]: I can't create a plate in the room}; return 0 };
			print $file $new_plate;
			close $file;

			unlink map { File::Spec->catfile($patefons_knobs{path_to_rooms}, $room_toChange, $_ ) } @plates;
			report_to_meloman("CHANGED: room=$room_toChange plate=$new_plate");
			return 1;
		} else { push @{$patefons_knobs{errors}}, qq{[change_the_plate]: I can't open the room}; return 0 }
	}

	sub listen_the_plate {
# 		$patefons_knobs{error} = '';
		my %report;

		my $maxIterations = ( $patefons_knobs{maxSleeping} / $patefons_knobs{sample_rate} );
		foreach ( my $i = 1; $i <= $maxIterations; $i++ ) {
			foreach my $room ( keys %nowPlaying ) {

				my $file = File::Spec->catfile($patefons_knobs{path_to_rooms}, $room, $nowPlaying{$room} );
				unless ( -e $file ) {
					( $report{$room}{new_plate}, $report{$room}{banderol} ) = somethig_happened( $room );
					return 0 unless ( $report{$room}{new_plate} );
				}
			}
			if ( %report ) { report_to_meloman( \%report ); return 1 }
			sleep $patefons_knobs{sample_rate};
		}
		report_to_meloman();
	}

	sub somethig_happened {

		my $room = shift;
		if ( opendir my $dir, File::Spec->catfile( $patefons_knobs{path_to_rooms}, $room ) ) {
# 			take plate with highes number
			my $playing_plate = (sort grep { /^\d+$/ } readdir $dir )[-1];
			unless ( $playing_plate ) { push @{$patefons_knobs{errors}}, qq{[listen_the_plate]: There is no plates at all}; return 0 }

			my $packed_banderol;
# 			start handler for this 'port' and give to it 'old plate number' and 'new plate number'
			if ( ref $patefons_knobs{handlers}{$room} eq 'CODE' ) {
				my %banderol = $patefons_knobs{handlers}{$room}( $nowPlaying{$room}, $playing_plate );
				if ( %banderol ) { $packed_banderol = pack_banderol( \%banderol ) }
			} else { push @{$patefons_knobs{errors}}, qq{[listen_the_plate]: It looks like handler for $room is not a sub} }

			return $playing_plate, $packed_banderol;
		} else { push @{$patefons_knobs{errors}}, qq{[listen_the_plate]: It seems there is no room}; return 0 }
	}

	sub pack_banderol {

		',"banderol":{' . join( ',', ( map {
			${$_[0]}{$_}=~s{\\}{\\\\}g;
			${$_[0]}{$_}=~s{"}{\\"}g;
			${$_[0]}{$_}=~s{,}{\\,}g;
			${$_[0]}{$_}=~s{:}{\\:}g;
			qq/"$_":"${$_[0]}{$_}"/ 
		} grep { ! ref(${$_[0]}{$_}) } keys %{$_[0]} )) . '}'
	}

	sub report_to_meloman {

		my $answer = 'NO CHANGES';
		if ( $_[0] ) {
			if ( ref $_[0] eq 'HASH' ) {
				$answer = '{' . join ( ',', ( map {
					'"' . $_ . '":{"plate":"' . ${$_[0]}{$_}{'new_plate'} . '"' . ${$_[0]}{$_}{'banderol'}
				} keys %{$_[0]} ) ) . '}}';
			}
			if ( ref $_[0] eq '' ) { $answer = $_[0] }
		}
		print qq(Content-Type: text/html\n\n$answer);
		return 1;
	}
}
1;