var meloman;

if ( !meloman ) meloman = {

	xmlhttp				: null,
	lastCalledTime_AutoListening	: null,
	activeListening			: null,			// process's id of last called AutoListening()
	nowPlaying			: {},			// hash — 'room' ( "port" to listen ) : 'current plate'
	sendedFlag			: false,

	// settings:
	minReconnectTime	: 2000,
	reconnectTime		: 20000,
	routeToChange		: '',			// path to server-script what makes changes
	routeToListen		: '',			// path to server-script what listens
	hands			: {},			// hash of links to external functions to start when "incoming connection" happens — 'room' : 'link'
	minHandLength		: 2000,			// ( miliseconds ) minimal time between applying function what is in 'hand' TODO make it between 'asks'
	connectionErrorHandler	: null,			// function for handling connections errors ( it starts when http answer status !== 200 )
	waitingTimeOutProcess	: null,
	waitingTimeOutHandler	: null,			// function for handling 'time out of waiting server's respond'
	waitingTimeOut		: 30000,		// maximum time ( in miliseconds ) for waiting server's respond
	ignoreMyChanges		: false,		// 

	ParseAnswer : function ( answer ) {
// console.log('ANSWER: --'+ answer + '--');
				meloman.sendedFlag = false;
			if ( answer == 'NO CHANGES' ) { meloman.AutoListening(); return }
// 			CHANGED: room=1 plate=11
			var S = /CHANGED: room=(\d+) plate=(\d+)/.exec( answer );
			if ( S ) {
				if ( ! meloman.ignoreMyChanges ) { if ( nowPlaying[S[1]] ) nowPlaying[S[1]] = S[2] }
				meloman.lastCalledTime_AutoListening = 0;
				meloman.AutoListening();
				return
			}

			var answer = eval ( '(' + answer + ')' );

// 			write new 'plates' to nowPlaying
			for ( var room in answer ) {
				if ( meloman.nowPlaying[room] !== undefined ) {
					meloman.nowPlaying[room] = answer[room]['plate']
				}
			}

			meloman.AutoListening();

// 			start functions from 'hands'
			for ( var room in answer ) {
				if ( typeof window[ meloman.hands[room] ] == 'function' ) {
					window[ meloman.hands[room] ]( answer[room]['banderol'] )
				}
			}
	},


	Change : function ( room, params ) {

		var paramsString = '';
		clearTimeout ( meloman.activeListening );

// 		test if 'room' exists and is a number
		if ( /\D/.test(room) ) throw new Error ('[ meloman.Change ]: first parametr ( room ) must be a number');
		if ( ! meloman.hands[room] ) throw new Error ( '[ meloman.Change ]: I\'m not listening port #' + room + ' now');

		var roomAndPlateString = 'changeThePlate=' + encodeURIComponent( '{"' + room + '":"' + meloman.nowPlaying [ room ] + '"}');

// 		pack parameters to banderol
		if ( params ) {
			if ( typeof params !== 'object' ) throw new Error ( '[ meloman.Change ]: second parameter must be an object');
			paramsString = '{';
			for ( var p in params ) {
// 				shield & and :							TODO we shouldnot do this! becouse of encodeURIcomponent
				var parameter	= p.replace( /,/g, "\\," );
				var myvalue	= params[p].replace( /,/g, "\\," );

				paramsString += '"' + parameter + '":"' + myvalue + '",';
			}
			paramsString = paramsString.replace( /,$/, '}' );
			paramsString = '&banderol=' + encodeURIComponent( paramsString );
		}

		if ( ! meloman.routeToChange ) throw new Error ( '[ meloman.Change ]: there is no route for changing plates');
		meloman.SendRequest ( meloman.routeToChange, roomAndPlateString + paramsString );
		meloman.sendedFlag = true;
		meloman.AutoListening();
	},


	Listen : function ( what ) {

		if ( ! meloman.xmlhttp ) meloman.xmlhttp = meloman.getXmlHttp();		// TODO define it with init

		clearTimeout ( meloman.activeListening );
		meloman.hands		= {};
		meloman.nowPlaying	= {};
		if ( ! what ) { return true }

// 		parse request
		what = what.replace( /\s+/g, '' );
		what = what.replace( /;{2,}/, ';' );
		what = what.replace( /^;+|;+$/g, '' );

		var pairs = what.split( ';' );
		for ( var each in pairs ) {
			var s = pairs[each].split(':');
			if ( s[1] == undefined || !/^\d+(?:-\d+)?$/.test( s[1] ) ) {
				throw new Error ('[ meloman.Listen ]: ('+s[0]+':'+s[1]+') -- check parameter sintax');
			}
			if ( typeof window[ s[0] ] !== 'function' ) {
				throw new Error ('[ meloman.Listen ]: '+s[0]+' is not a function');
			}
			if ( /^\d+$/.test( s[1] ) ) {
				if ( meloman.hands[ s[1] ] ) {
					throw new Error ('[ meloman.Listen ]: overlapping sets of "ports"');
				}
				meloman.hands[ s[1] ] = s[0];
			} else {
				var k = s[1].split('-');
				for ( var i = (k[0]*1); i <= (k[1]*1); i++ ) {
					if ( meloman.hands[ i ] ) {
						throw new Error ('[ meloman.Listen ]: overlapping sets of "ports"');
					}
					meloman.hands[ i ] = s[0];
				}
			}
		}
		for ( var room in meloman.hands ) meloman.nowPlaying [ room ] = 0;
// 		start listening
		meloman.AutoListening();
	},



	AutoListening : function () {

		clearTimeout ( meloman.activeListening );

		var isnowPlayingEmpty = true;
		for ( var i in meloman.nowPlaying ) { isnowPlayingEmpty = false; break }
		if ( isnowPlayingEmpty ) return false;

		if ( ! meloman.routeToListen ) throw new Error ('[ meloman.AutoListening ]: there is no route to listen');
		if ( ! meloman.sendedFlag ) {
// 			avoid too frequent requests. min is — meloman.minReconnectTime
			var timeNow = new Date().getTime();
			var timeDiff = timeNow - meloman.lastCalledTime_AutoListening;
			if ( timeDiff < meloman.minReconnectTime ) { 
				meloman.activeListening = setTimeout ( 'meloman.AutoListening()', timeDiff + 1 );
				return false;
			}

// 			glue and send reqest
			var currentState = '{';
			for ( var room in meloman.nowPlaying ) currentState += '"' + room + '":"' + meloman.nowPlaying[room] + '",';
			currentState = currentState.replace( /,$/, '}');
			currentState = 'nowPlaying=' +  encodeURIComponent( currentState );
			meloman.SendRequest ( meloman.routeToListen, currentState );
		}
// 		auto restarting
		meloman.activeListening = setTimeout( 'meloman.AutoListening()', meloman.reconnectTime);
		meloman.lastCalledTime_AutoListening = new Date().getTime();
	},



	SendRequest : function ( where, what ) {
// console.log ( where + ' : ' + what );
		if ( meloman.xmlhttp.readyState > 0 ) meloman.xmlhttp.abort();
// 		meloman.xmlhttp = meloman.getXmlHttp();
		meloman.xmlhttp.open ( "POST", where, true );
		meloman.xmlhttp.setRequestHeader ( "Content-Type", "application/x-www-form-urlencoded;charset=utf-8" );
		meloman.xmlhttp.onreadystatechange=function () {
			if ( meloman.xmlhttp.readyState != 4 ) return;

			if ( typeof window[meloman.waitingTimeOutHandler] == 'function' ) clearTimeout ( meloman.waitingTimeOutProcess );
			if ( meloman.xmlhttp.status == 200 ) {
				meloman.ParseAnswer ( meloman.xmlhttp.responseText );
			} else if ( typeof window[meloman.connectionErrorHandler] == 'function' ) {
				window[meloman.connectionErrorHandler](meloman.xmlhttp.status)
			}
		}
		if ( typeof window[meloman.waitingTimeOutHandler] == 'function' ) {
			meloman.waitingTimeOutProcess = setTimeout ( 'window[meloman.waitingTimeOutHandler]', meloman.waitingTimeOut )
		}
		meloman.xmlhttp.send ( what );
// console.log ( what );
	},



	getXmlHttp : function () {
	
		var xmlhttp;
		try {
			xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (err) {
			try {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (err) {
				xmlhttp = false;
			}
		}
		if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
			xmlhttp = new XMLHttpRequest();
		}
		return xmlhttp;
	}

}